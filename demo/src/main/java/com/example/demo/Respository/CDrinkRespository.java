package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CDrink;

public interface CDrinkRespository extends JpaRepository <CDrink , Long> {
    
}
