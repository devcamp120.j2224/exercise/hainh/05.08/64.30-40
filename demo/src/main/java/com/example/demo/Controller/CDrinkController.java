package com.example.demo.Controller;

import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CDrink;
import com.example.demo.Respository.CDrinkRespository;
import com.example.demo.Service.CDrinkService;

@RestController
@CrossOrigin
public class CDrinkController {
    
    @Autowired
    CDrinkService cDrinkService ;

    @Autowired
    CDrinkRespository cDrinkRespository ;

    // api lấy danh sách drink
    @GetMapping("drinks")
    public ResponseEntity<List<CDrink>> getAllDrinkService() {
        try {
            return new ResponseEntity<>(cDrinkService.getDrinkList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }

    // ko dc để trùng tên 
    // api tìm id trong list drink
    @GetMapping("drink-info")
    public ResponseEntity<Object> getDrinkByIdService(@RequestParam(name = "id") long id) {
        try {
            CDrink drink = cDrinkService.getDrinkById(id);
            
    		if (drink != null) {
    		    return new ResponseEntity<>(drink, HttpStatus.OK);	
    		} 
    		else {
    			return ResponseEntity.badRequest().body("Failed to get specified Drink: "+id + "  for update.");
    		}
    	}
        catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Drink:"+e.getCause().getCause().getMessage());
            }
        }


    // api tạo 1 đối tượng mới
    @PostMapping("drink-create")// Dùng phương thức POST
	public ResponseEntity<Object> createDrinkService(@Valid @RequestBody CDrink pDrinks) {
		try {
			
			CDrink drink = cDrinkService.createDrinkById(pDrinks);
            if (drink != null) {
    		    return new ResponseEntity<>(drink, HttpStatus.CREATED);	
    		} 
    		 else {
    			return ResponseEntity.unprocessableEntity().body(" Drink already exsit");
    		}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    // api update 1 đối tượng
    @PutMapping("drink-update")// Dùng phương thức PUT
     	public ResponseEntity<Object> updateDrinkByIdService(@Valid @RequestParam("id") long id, @RequestBody CDrink pDrinks) {
     		CDrink drink = cDrinkService.updateDrinkById(id, pDrinks);
 
    		if (drink != null) {
    			try {
    				return new ResponseEntity<>(cDrinkRespository.save(drink), HttpStatus.OK);	
    			} catch (Exception e) {
    				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
    			}
    		} else {
    			return ResponseEntity.badRequest().body("Failed to get specified Voucher: "+id + "  for update.");
    		}
     	}
    // api delete theo id
    
    @DeleteMapping("drink-delete/{id}")// Dùng phương thức DELETE
        public ResponseEntity<CDrink> deleteCVoucherById(@PathVariable("id") long id) {
            try {
                cDrinkRespository.deleteById(id);
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } 
            catch (Exception e) {
                System.out.println(e);
                    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
    }
    
    // api xóa tất cả id 

        // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("drink-delete-all")// Dùng phương thức DELETE
        public ResponseEntity<CDrink> deleteAllCVoucher() {
            try {
                cDrinkRespository.deleteAll();
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } 
            catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
}
