package com.example.demo.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.CDrink;
import com.example.demo.Respository.CDrinkRespository;

@Service
public class CDrinkService {

                    @Autowired
                    CDrinkRespository cDrinkRespository;


                    // hàm lấy tất cả list 
        public ArrayList<CDrink> getDrinkList() {
            ArrayList<CDrink> drinkList = new ArrayList<>();
                cDrinkRespository.findAll().forEach(drinkList::add);
                    return drinkList;
        }

        // hàm tìm id 
        public CDrink getDrinkById(long id) {
            Optional<CDrink> drinkData = cDrinkRespository.findById(id);
                if (drinkData.isPresent()) {
                    CDrink cDrink = drinkData.get();
                        return cDrink;
                    }
                        else{
                            return null;
                    }
        }

        // hàm tạo phải làm ngược lại 
        public CDrink createDrinkById(CDrink pDrinks) {
            Optional<CDrink> drinkData = cDrinkRespository.findById(pDrinks.getId());
                if(drinkData.isPresent()) {
                    return null ;
                }
                else{
                    pDrinks.setNgayTao(new Date());
                    pDrinks.setNgayCapNhat(null);
                    CDrink drinks = cDrinkRespository.save(pDrinks);
                return drinks;
            }
        }


        // hàm update dữ liệu 

        public CDrink updateDrinkById(long id, CDrink pDrinks) {
            Optional<CDrink> drinkData = cDrinkRespository.findById(id);
                if (drinkData.isPresent()) {
                    CDrink drink = drinkData.get();
                        drink.setDonGia(pDrinks.getDonGia());
                        drink.setMaNuocUong(pDrinks.getMaNuocUong());;
                        drink.setTenNuocUong(pDrinks.getTenNuocUong());
                        drink.setNgayCapNhat(new Date());
                return drink;
            }
                    else{
                        return null;
                    }
        }
    }


